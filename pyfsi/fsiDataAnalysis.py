# FSI interferometer data analysis
# Description: Script used to conver the raw data from the FSI intergerometer to distance value in single target configuration
# Version: 1.1
# Author: J.Rutkowski
# Date: 11/2019
# CERN 2019

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import peakutils
from pyfsi.fsiFunctions import DataLinearize, filterDataButterworthBandpass, gasCellFindPeaks, fitFsiPeaks
import ref_index
from pyfsi.fsiEnums import *
import time
from scipy.signal import savgol_filter
import logging
from pyfsi.fsiExceptions import *


def fsiDataAnalysis(rawDataStorage, fsiParamsHandle, GlobalConfig, envCond, fullOutput=False):
    '''
    Analyze the FSI data and return distance values.

    Keyword arguments:
    ------------------
    rawDataStorage  -- dictionary with the following keys:
                    ['Measurement int'] - measurement interferometer raw data
                    ['Gas cell']        - gas cell raw data
                    ['Reference int']   - reference interferometer raw data
                    ['Time interval']   - time interval between samples

    fsiParamsHandle -- channel parameters - object of class FsiSignalParams
    GlobalConfig    -- class containing the setup configuration
    envCond         -- dictionary containing the environmental conditions, eg. {'T': 20.0, 'H': 40.0, 'P': 96750} (deg C, %, Pa). If empty or incomplete the distance is calculated for a refractive index = 0.
    fullOutput      -- if True returns raw data, FFT, fit and gas cell identified peaks.

    Returns:
    --------
    if fullOutput==False: 
        distancePeakValues, warningMsg  -- array with the distance values for the current channel. Length depends on the length of chanParams.currentDistance since muliple peaks can be detected for single channel.
    if fullOutput==True: 
        processedDataStorage, warningMsg
        processedDataStorage is a dictionary, containing the following keys:
            ['Filtered gas cell out']     - Processed and filtered gas cell ouptut values (np.array)
            ['Gas cell peak idxs']        - Indexes of the peaks identified in the gas cell spectrum (np.array)
            ['FFT frequency axis']        - FFT x-axis with the frequency values (np.array)
            ['FFT distance axis']         - FFT x-axis with the distance values (np.array)
            ['FFT magnitude axis']        - FFT y-axis with the magnitude data (np.array)
            ['FFT fit peaks distance']    - Dictionary, containing x-axis data of the Lorenzian-type curve fitted to the FFT peak. One key is created for every distance present in fsiParamsHandle.currentDistance (Peak 1, Peak 2, ...etc)
            ['FFT fit peaks magnitude']   - Dictionary, containing y-axis data of the Lorenzian-type curve fitted to the FFT peak. One key is created for every distance present in fsiParamsHandle.currentDistance (Peak 1, Peak 2, ...etc)
            ['Fitted distance values']    - # array containing the center values of the FFT fit for each peak. This is considered as the distance (output) measured by the FSI interferometer! (np.array)

    '''
    retStatus = {}              # Returned status message
    processedDataStorage = {}   # Dictionary for processed data storage

    # CONSTANTS
    wavelength  = 1550.0                                    # Wavelength (nm)
    cLight      = 299792458                                 # Speed of light in m/s
    zpf         = 1 #int(GlobalConfig.computationConfig.ZPF) + 1    # Zero padding factor (for FFT) - if '1' no zero padding

    # Refractive index calculation
    if(('T' not in envCond) or ('H' not in envCond) or ('P' not in envCond)):
        nAir = 1.0
    else:
        nAir = ref_index.ciddor(wavelength, envCond['T'], envCond['P'], envCond['H'])  # Refractive index of air for given conditions (Ciddor equation) 

    # DEBUG: Saving raw acquired data
    #if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SAVE_RAW_DATA)):
     #   np.save('Debug\\' + str(time.time()) + '_RAW_DATA_CH' + str(fsiParamsHandle.chanNumber) + '.npy', np.array([np.float64(rawDataStorage['Reference int']), np.float64(rawDataStorage['Measurement int']), np.float64(rawDataStorage['Gas cell']), np.float64(rawDataStorage['Time interval'])]))
    
    #rawDataStorage['Time interval'] = rawDataStorage['Time interval'] * 1e-9
    
    #print("dupa")
    print("Tinterval: " + str(rawDataStorage['Time interval']) )

    # DEBUG: Displays raw data
    #if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SHOW_RAW_DATA) or (GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE)):
        #print('Tinterval: ' + str(rawDataStorage['Time interval']) + ' s')
        #fig = plt.figure()
        #plt.subplot(3, 1, 1)
        #plt.plot(rawDataStorage['Reference int'])
        #plt.subplot(3, 1, 2)
        #plt.plot(rawDataStorage['Measurement int'])
        #plt.subplot(3, 1, 3)
        #plt.plot(rawDataStorage['Gas cell'])
        #plt.show()


    # DATA LINEARISATION
    try:
        GAS_CELL_out, MEAS_out = DataLinearize(rawDataStorage['Time interval'], rawDataStorage['Reference int'], rawDataStorage['Gas cell'], rawDataStorage['Measurement int'])
    except Exception as e:
        logging.exception('Linearisation error: ' + str(e))
        raise FsiLinearizationException('Linearisation error: ' + str(e))

    #if(GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE):
     #   plt.plot(MEAS_out)
      #  plt.show()

    return


    # SWEEP SPEED CORRECTION USING GAS CELL DATA
    try:
        # Thorlabs photodetectors operate in photovoltaic mode so the signal is inversed
        if(GlobalConfig.photoModuleParams.VERSION == 'Thorlabs' and not ((GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE) and (GlobalConfig.externalRawData[-3:] == 'mat'))):
            GAS_CELL_out = GAS_CELL_out*(-1)
        
        processedDataStorage['Filtered gas cell out'] = savgol_filter(GAS_CELL_out, 101, 2, mode='mirror')  # Signal filtering - Savitzky-Golay filter
        processedDataStorage['Gas cell peak idxs'], retStatus['Gas cell'] = gasCellFindPeaks(processedDataStorage['Filtered gas cell out'], GlobalConfig.picoParams, GlobalConfig.laserParams, 'pyfsi\\SRM2519a.csv')  # Beta test

        GAS_CELL_wavelengths = np.genfromtxt('pyfsi\\SRM2519a.csv')                 # Load reference data for the gas cell used (NIST)
        GAS_CELL_wavelengths = GAS_CELL_wavelengths*1e-9                            # Convert to m
        GAS_CELL_spectrum = cLight/GAS_CELL_wavelengths                             # Convert reference spectral data to frequency
        GAS_CELL_spectrum_time = processedDataStorage['Gas cell peak idxs']*rawDataStorage['Time interval']                  # Calculate peak occurence times in the measured gas cell signal
    
        alpha = np.polyfit(GAS_CELL_spectrum_time, GAS_CELL_spectrum,1)             # Calculate the real sweep speed (linear fit)
        alpha = alpha[0]
    
    except Exception as e:
        logging.exception('Gas cell analysis error' + str(e))
        raise FsiGasCellPeakSearchException('Gas cell fitting exception: ' + str(e))


    # Show gas cell fitting plots
    if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SHOW_GAS_CELL) or (GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE)):
        plt.plot(GAS_CELL_out)
        plt.plot(processedDataStorage['Filtered gas cell out'])
        plt.plot(processedDataStorage['Gas cell peak idxs'], processedDataStorage['Filtered gas cell out'][processedDataStorage['Gas cell peak idxs']], 'x')
        plt.show()


    # MEASUREMENT INTERFEROMETER DATA ANALYSIS
    try:
        processedDataStorage['FFT magnitude axis'] = abs(np.fft.rfft(MEAS_out, int(len(MEAS_out)+ (zpf-1)*len(MEAS_out))))        # "Real" FFT, length includes padded zeros
        f = np.fft.rfftfreq( int(len(MEAS_out) + (zpf-1)*len(MEAS_out)), d=rawDataStorage['Time interval'])        # Frequency axis calculation, length includes padded zeros
        Meas_coef = cLight/(2*abs(alpha)*nAir)                                               # Frequency-to-distance calculation coefficient
        processedDataStorage['FFT distance axis'] = Meas_coef*f                                                                    # Matrix containing calculated distances
    except Exception as e:
        logging.exception('FFT calculation error: ' + str(e))
        raise FsiMeasurementIntFftCalcException('FFT calculation exception: ' + str(e))

    # Conditional return statements
    if(fullOutput == False): 
        # PEAK FITTING - only distance value(s)
        processedDataStorage['Fitted distance values'], retStatus['Fft fit'] = fitFsiPeaks(processedDataStorage['FFT distance axis'], processedDataStorage['FFT magnitude axis'], fsiParamsHandle, GlobalConfig.setupOpMode, fullOutput)

        return processedDataStorage['Fitted distance values'], retStatus

    else:
        # PEAK FITTING - full output
        processedDataStorage['FFT fit peaks distance'], processedDataStorage['FFT fit peaks magnitude'], processedDataStorage['Fitted distance values'], retStatus['Fft fit'] = fitFsiPeaks(processedDataStorage['FFT distance axis'], processedDataStorage['FFT magnitude axis'], fsiParamsHandle, GlobalConfig.setupOpMode, fullOutput)

        return processedDataStorage, retStatus
        #return processedDataStorage['Filtered gas cell out'], processedDataStorage['Gas cell peak idxs'], processedDataStorage['FFT distance axis'], processedDataStorage['FFT magnitude axis'], processedDataStorage['FFT fit peaks distance'], processedDataStorage['FFT fit peaks magnitude'], processedDataStorage['Fitted distance values'], retStatus
