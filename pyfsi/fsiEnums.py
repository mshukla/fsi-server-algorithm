# Enums related with the Pico Scope 3000
class PICO_CHANNEL:     # Enum for Pico Scope 3000/4000 channel numbers
    A   = 0
    B   = 1
    C   = 2
    D   = 3
    E   = 4 # In PS 3000 series this is an external input in PS 4000 this is channel E
    F   = 5
    G   = 6
    H   = 7

class PICO_RANGE:       # Enum for Pico Scope 3000 channel voltage ranges
    R10MV   = 0
    R20MV   = 1
    R50MV   = 2
    R100MV  = 3
    R200MV  = 4
    R500MV  = 5
    R1V     = 6
    R2V     = 7
    R5V     = 8
    R10V    = 9
    R20V    = 10
    R50V    = 11
    MAX     = 12

class PICO_COUPLING:    # Enum for Pico Scope 3000 channel coupling
    AC = 0
    DC = 1

class TRIGGER_EDGE:     # Enum for Pico Scope 3000 trigger threashold detection
    ABOVE               = 0
    BELOW               = 1
    RISING              = 2
    FALLING             = 3
    RISING_OR_FALLING   = 4
    ABOVE_LOWER         = 5
    BELOW_LOWER         = 6
    RISING_LOWER        = 7
    FALLING_LOWER       = 8

class SAMPLING_RATE:    # Enum for Pico Scope 3000 sampling rates
    R250MSPS    = 2
    R125MSPS    = 3
    R62M5SPS    = 4
    R31M25SPS   = 6

class SAMPLING_RATE_PS4000:    # Enum for Pico Scope 4000 sampling rates
    R80MSPS    = 0
    R40MSPS    = 1
    R20MSPS    = 3
    R10MSPS    = 7

# Enums related with the TLM-8700 laser module
class LASER_INTERLOCK: # SW interlock only
    UNLOCK = 0
    LOCK   = 1

class LASER_INTERLOCKS: # Both HW and Software interlocks
    BOTH_UNLOCKED           = 0
    HW_UNLOCKED_SW_LOCKED   = 1
    HW_LOCKED_SW_UNLOCKED   = 2
    BOTH_LOCKED             = 3

class LASER_STATE:
    ON  = 1
    OFF = 0

class LASER_ERR:
    OK   = 0
    COMM = 1

class LASER_PWR_UNIT:
    DBM = 0
    MW  = 1

class LASER_DOMAIN:
    WAVELENGTH = 0   # nm units
    FREQUENCY = 1   # GHz units

class LASER_MOD_SOURCE:
    NO_MOD    = 0
    COHERENCE = 1
    EXT       = 3

class  LASER_SCAN_MODE:
    AUTO_STEP     = 1
    UNI_FORWARD   = 2
    BIDIRECTIONAL = 3
    UNI_REVERSE   = 4

class TRIG_POLARITY:
    ACTIVE_LOW  = 0
    ACTIVE_HIGH = 1


# Enums related with the Sercalo 1x16 Fiber Optic Switch Module
class FO_SWITCH_ERR:
    OK   = 0
    COMM = 1

# Parameters used for calculation of the distance from the acquired FSI signal
class FsiSignalParams:
    def __init__(self, CHAN_NUM = 0, CURRENT_DISTANCE = 0.5, DISTANCE_WINDOW = 0.01, PICO_V_RANGE = PICO_RANGE.R200MV, ERROR_COUNTER = 0, FBG = False):
        self.distanceWindow    = DISTANCE_WINDOW    # Distance around center value for the fitting window 
        self.currentDistance   = CURRENT_DISTANCE   # Current value of distance measurement
        self.chanNumber        = CHAN_NUM           # Channel number
        self.picoRange         = PICO_V_RANGE       # Current voltage range of the PicoScope channel
        self.errorCounter      = ERROR_COUNTER      # Error counter
        self.fbg               = FBG                # Defines whether the channel is a Fiber Bragg Grating sensor of an FSI sensor (Boolean)

# Enums related with the Keopsys EDFA amplifier
class EDFA_STATE:
    ON  = 2
    OFF = 0

class EDFA_ERR:
    OK   = 0
    COMM = 1

# Enums related with the Photodetection Module
class PHOTO_MODULE_CHANNEL:
    ALL = 0
    CH1 = 1
    CH2 = 2
    CH3 = 3
    CH4 = 4
    CH5 = 5
    CH6 = 6
    CH7 = 7
    CH8 = 8
    CH9 = 9
    CH10 = 10
    CH11 = 11
    CH12 = 12
    CH13 = 13
    CH14 = 14
    CH15 = 15
    CH16 = 16
    REF_INT = 17
    GAS_CELL = 18
    BANK2 = 19

class PHOTO_MODULE_CH_STATE:
    ON  = 1
    OFF = 0

class PHOTO_MODULE_ERR:
    OK   = 0
    COMM = 1

class PHOTO_MODULE_CH_GAIN:
    G5K  = 5
    G20K = 20

class SETUP_OP_MODE:
    NORMAL                  = 0
    DEBUG_SHOW_PEAKS        = 1
    DEBUG_SHOW_GAS_CELL     = 2
    DEBUG_SAVE_PEAKS        = 3
    DEBUG_SHOW_SAVE_PEAKS   = 4
    DEBUG_SAVE_RAW_DATA     = 5
    DEBUG_SHOW_RAW_DATA     = 6
    TEST_MODE               = 7
    DEBUG_SHOW_FFT          = 8
    DEBUG_SHOW_SAVE_FFT     = 9

