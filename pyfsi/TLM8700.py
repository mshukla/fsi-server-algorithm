# TLM-8700 laser module RS-232/USB interface functions, based on the "legacy" protocol
# Author: J.Rutkowski
# Revision: 1.0
# CERN 11/2019
# Change log:
# - Python 3.x - compatible version

import serial
import time
from pyfsi.fsiEnums import *

debugTLM8700Status = {}


def read_Line(serH):
    """
    Reads a line from the serial port, the TLM-8700 laser module terminates each line with <LF><CR> instead of <CR><LF> therefore the standard pySerial readline() function cannot be used...

    Keyword arguments:
    ------------------
    serH -- handle to the laser's serial port

    Return:
    -------
    str -- read line
    """
    str = ""
    while 1:
        ch = (serH.read()).decode()
        if(ch == '\n' or ch == ''):
            serH.read()                 # Reads and neglects the last <CR> from the serial buffer, otherwise it would get read during another call
            break
        str += ch
    return str


class Tlm8700Wrapper:

    def __init__(self, comPort):
        """
        Opens serial connection with the laser module.

        Keyword arguments:
        ------------------
        comPort -- laser COM port (string)
        """
        self.ser = serial.Serial(comPort, 115200, timeout=1)


    def disconnect(self):
        """
        Closes the serial connection with the laser module.
        """
        self.ser.close()


    def checkIdentity(self):
        """
        Checks whether the device is actually the TLM8700 laser module.

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('id?\n'))
        received = (read_Line(self.ser)).split(';')
        if(received[1] == 'TLM8700'):
            ret = LASER_ERR.OK
        return ret


    def readId(self):
        """
        Reads and returns the laser ID.

        Return:
        -------
        received -- laser ID string
        """
        self.ser.write(str.encode('id?\n'))
        received = read_Line(self.ser)
        return received

    
    def setLaserState(self, lState):
        """
        Turns ON or OFF the laser.

        Keyword arguments:
        ------------------
        lState -- laser state - LASER_STATE type

        Return:
        -------
        ret -- Error status LASER_ERR type

        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('laz ' + str(lState) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def getLaserInterlockState(self):
        """
        Get the status of the laser HW and SW interlocks.

        Returns:
        --------
        ret -- status ow the laser interlocks - LASER_INTERLOCKS-type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('int?\n'))
        received = int(read_Line(self.ser))
        if ((received == LASER_INTERLOCKS.BOTH_LOCKED) or (received == LASER_INTERLOCKS.BOTH_UNLOCKED) or (received == LASER_INTERLOCKS.HW_LOCKED_SW_UNLOCKED) or (received == LASER_INTERLOCKS.HW_UNLOCKED_SW_LOCKED)):
            ret = received
        return ret
        
   
    def setLaserInterlock(self, iLock):
        """
        Turns ON or OFF the laser interlock

        Keyword arguments:
        ------------------
        iLock -- interlock state - LASER_INTERLOCK type

        Return:
        -------
        ret -- Error status LASER_ERR type

        Exceptions:
        -----------
        Raises a ValueError exception if the HW interlock is locked
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('int ' + str(iLock) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        lockState = self.getLaserInterlockState()
        if((lockState == LASER_INTERLOCKS.HW_LOCKED_SW_UNLOCKED) or (lockState == LASER_INTERLOCKS.BOTH_LOCKED)):
            raise ValueError('Laser module hardware interlock is locked, please unlock with dedicated key!')
        return ret

   
    def setLaserPower(self, lPower):
        """
        Sets the laser output power.

        Keyword arguments:
        ------------------
        lPower -- laser power value either in mW or in dBm (depending on setPowerUnit() setting) either integer or float type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('pwr ' + str(lPower) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setPowerUnit(self, powerU):
        """
        Sets the laser output power unit.

        Keyword arguments:
        ------------------
        powerU -- unit - LASER_PWR_UNIT type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('pwru ' + str(powerU) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setLaserTuningDomain(self, lDomain):
        """
        Sets the laser tuning domain - either nm or GHz (wavelength or frequency).

        Keyword arguments:
        ------------------
        lDomain -- tuning domain - LASER_DOMAIN type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('unit ' + str(lDomain) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setModulationSource(self, mSource):
        """
        Sets the laser modulation source

        Keyword arguments:
        ------------------
        mSource -- laser modulation source - LASER_MOD_SOURCE type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('sms ' + str(mSource) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret
    

    def setSetScanStart(self, sStart):
        """
        Sets the laser scan start wavelength or frequency - depending on setLaserTuningDomain() setting

        Keyword aruments:
        -----------------
        sStart -- value in nm or GHz, either integer or floating point

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('str ' + str(sStart) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret

    
    def setSetScanStop(self, sStop):
        """
        Sets the laser scan end wavelength or frequency - depending on setLaserTuningDomain() setting

        Keyword arguments:
        ------------------
        sStop -- value in nm or GHz, either integer or floating point

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('stop ' + str(sStop) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setScanMode(self, sMode):
        """
        Sets the laser scan mode.

        Keyword arguments:
        ------------------
        sMode -- laser scan mode LASER_SCAN_MODE type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('mode ' + str(sMode) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setScanSpeed(self, sSpeed):
        """
        Sets the laser scan speed.

        Keyword arguments:
        ------------------
        sSpeed -- Laser scan speed in nm/s (wavelength domain) or GHz/s (frequency domain), both integer type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('spd ' + str(sSpeed) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setScanDwellTime(self, dTime):
        """
        Sets the laser scan dwell time - time between two consecutive scans.

        Keyword arguments:
        ------------------
        dTime -- Dwell time in ms (integer)

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('dwl ' + str(dTime) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def setScanCycles(self, sCycles):
        """
        Sets the number of laser scan cycles.

        Keyword arguments:
        ------------------
        sCycles -- number of cycles - integer value: 1 =< sCycles => 5000000 or '-1' for infinite number of cycles

        Return:
        ret -- Error status LASER_ERR type
        -------
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('num ' + str(sCycles) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret

    
    def startScan(self):
        """
        Start the laser scan.

        Return
        ------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('scan\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def readExtTemp(self):
        """
        Reads and returns the environmental temperature.

        Return:
        -------
        received -- environmental temperature in deg C
        """
        self.ser.write(str.encode('tmpe?\n'))
        received = read_Line(self.ser)
        return received
        

    def setTriggerPolarity(self, tPolarity):
        """
        Sets the trigger ans SYNC polarity

        Keyword arguments:
        ------------------
        tPolarity -- trigger and SYNC polarity TRIG_POLARITY type

        Return:
        -------
        ret -- Error status LASER_ERR type
        """
        ret = LASER_ERR.COMM
        self.ser.write(str.encode('trpol ' + str(tPolarity) + '\n'))
        received = read_Line(self.ser)
        if(received == '*'):
            ret = LASER_ERR.OK
        return ret


    def getUpTime(self):
        """
        Returns the operating hours of the laser diode

        Return:
        -------
        received -- number of hours the diode has been operating
        """
        self.ser.write(str.encode('ophours?\n'))
        received = read_Line(self.ser)
        return received


    def configureLaser(self, initParams):
        """
        Realize the sequence used to initialize the laser and set necessary parameters

        Keyword arguments:
        ------------------
        initParams - values of initialization parameters
        """
        debugTLM8700Status["readID"]        = self.readId()
        debugTLM8700Status["setInterlock"]  = self.setLaserInterlock(initParams.INTERLOCK)
        debugTLM8700Status["lState"]        = self.setLaserState(initParams.STATE)
        debugTLM8700Status["setUnit"]       = self.setPowerUnit(initParams.UNIT)
        debugTLM8700Status["setPower"]      = self.setLaserPower(initParams.POWER)
        debugTLM8700Status["setDomain"]     = self.setLaserTuningDomain(initParams.DOMAIN)
        debugTLM8700Status["setModSource"]  = self.setModulationSource(initParams.MOD_SOURCE)
        debugTLM8700Status["setStart"]      = self.setSetScanStart(initParams.START)
        debugTLM8700Status["setStop"]       = self.setSetScanStop(initParams.STOP)
        debugTLM8700Status["setScanMode"]   = self.setScanMode(initParams.SCAN_MODE)
        debugTLM8700Status["setScanSpeed"]  = self.setScanSpeed(initParams.SCAN_SPEED)
        debugTLM8700Status["setScanCycles"] = self.setScanCycles(initParams.CYCLES)
        debugTLM8700Status["setScanCycles"] = self.setTriggerPolarity(initParams.TRIGGER_POL)
        self.startScan()                    # First scan during initialization - otherwise after powering on, the laser 'hangs' for the first time. This is a workaround, casue should be found, contact supplier.
        time.sleep(0.1)
