# Definitions of exceptions used by the FSI algorithm
# Version: 1.0
# Author: J.Rutkowski
# Date: 12/2019
# CERN 2019

class FsiExceptionCounter:
    """ Storage for variables, holding the FSI exception counters """
    gasCellPeakSearchExcept     = 0
    fftCalcFailedExcept         = 0
    fftFittingFailedExcept      = 0
    fftFittingConfidenceExcept  = 0
    linearizationExcept         = 0
    sampleOutOfRangeExcept      = 0
    largeSampleJumpWarning      = 0
    fsiPeaksOverlapingWarning   = 0

class FsiGasCellPeakSearchException(Exception):
    """ Raise when a problem occurs with the gas cell peak search algorithm """
    pass

class FsiLaserLockedException(Exception):
    """ Raise when the laser module is HW-locked """
    pass

class FsiLinearizationException(Exception):
    """ Raise when there is a data linearization problem """
    pass

class FsiMeasurementIntFftCalcException(Exception):
    """ Raise when there is a problem with the Fourier transform calculation """
    pass

class FsiGeneralException(Exception):
    """ General exception, raise whenever there is a general problem in the algorithm and the result is corrupted """
    def __init__(self, exDisplayString):
        self.exDisplayString = exDisplayString
        print(exDisplayString)
    pass
