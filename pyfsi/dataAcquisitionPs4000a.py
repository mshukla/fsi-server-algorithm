# Acquisition from PicoScope using Python
# Test program - 3 channels acquisition triggered by the 4th channel
# For use with Series 4000 PicoScopes
# J.Rutkowski
# CERN 03/2020

import ctypes
from picosdk.ps4000a import ps4000a as ps
import numpy as np
import matplotlib.pyplot as plt
from picosdk.functions import adc2mV, assert_pico_ok
import pyfsi.fsiEnums
# test:
from pyfsi.TLM8700 import *
import time
# end test

chandle = ctypes.c_int16()                                                  # PicoScope device handle

def picoScopeOpen():
    openStatus = ps.ps4000aOpenUnit(ctypes.byref(chandle), None)
    changePowerSourceStatus = int(0)
    try:
        assert_pico_ok(openStatus)
    except:
        powerstate = openStatus

        if powerstate == 282:
            changePowerSourceStatus = ps.ps4000aChangePowerSource(chandle, 282)
        elif powerstate == 286:
            changePowerSourceStatus = ps.ps4000aChangePowerSource(chandle, 286)
        else:
            raise

        assert_pico_ok(changePowerSourceStatus)

    return openStatus, changePowerSourceStatus

def picoScopeClose():
    closeStatus = ps.ps4000aCloseUnit(chandle)
    assert_pico_ok(closeStatus)
    return closeStatus

def picoScopeSetChannel(chanNum, enable, coupling, chRange, offset):
    setStatus = ps.ps4000aSetChannel(chandle, chanNum, enable, coupling, chRange, offset)
    assert_pico_ok(setStatus)
    return setStatus

def picoScopeSetTrigger(picoInit):
    triggerStatus = ps.ps4000aSetSimpleTrigger(chandle, picoInit.PICO_TRIGGER_ENABLE, picoInit.PICO_TRIGGER_CH, picoInit.PICO_TRIGGER_THRESHOLD, picoInit.PICO_TRIGGER_EDGE, picoInit.PICO_TRIGGER_DELAY, picoInit.PICO_TRIGGER_TIMEOUT)
    assert_pico_ok(triggerStatus)
    return triggerStatus

def acquireData(laserHandle, picoInit):

    picoInit.PICO_NUMBER_OF_SAMPLES
    picoInit.PICO_SAMPLING_RATE

    timeIntervalns = ctypes.c_float()
    returnedMaxSamples = ctypes.c_int16()
    getTimebaseStatus = ps.ps4000aGetTimebase2(chandle, picoInit.PICO_SAMPLING_RATE, picoInit.PICO_NUMBER_OF_SAMPLES, ctypes.byref(timeIntervalns), ctypes.byref(returnedMaxSamples), 0)
    assert_pico_ok(getTimebaseStatus)
    runBlockStatus = ps.ps4000aRunBlock(chandle, 0, picoInit.PICO_NUMBER_OF_SAMPLES, picoInit.PICO_SAMPLING_RATE, None, 0, None, None)
    assert_pico_ok(runBlockStatus)

    bufferAMax = (ctypes.c_int16 * picoInit.PICO_NUMBER_OF_SAMPLES)()
    bufferBMax = (ctypes.c_int16 * picoInit.PICO_NUMBER_OF_SAMPLES)()
    bufferCMax = (ctypes.c_int16 * picoInit.PICO_NUMBER_OF_SAMPLES)()

    setBufChAStatus = ps.ps4000aSetDataBuffers(chandle, picoInit.PICO_MEAS_CH, ctypes.byref(bufferAMax), None, picoInit.PICO_NUMBER_OF_SAMPLES, 0, 0)
    assert_pico_ok(setBufChAStatus)

    setBufChBStatus = ps.ps4000aSetDataBuffers(chandle, picoInit.PICO_GAS_CELL_CH, ctypes.byref(bufferBMax), None, picoInit.PICO_NUMBER_OF_SAMPLES, 0, 0)
    assert_pico_ok(setBufChBStatus)

    setBufChCStatus = ps.ps4000aSetDataBuffers(chandle, picoInit.PICO_REF_CH, ctypes.byref(bufferCMax), None, picoInit.PICO_NUMBER_OF_SAMPLES, 0, 0)
    assert_pico_ok(setBufChCStatus)

    overflow = (ctypes.c_int16 * 10)()                              # Creates an overlow location for data
    cmaxSamples = ctypes.c_int32(picoInit.PICO_NUMBER_OF_SAMPLES)   # Creates converted types number of samples

    dataReady = ctypes.c_int16(0)
    dataCheck = ctypes.c_int16(0)
    time.sleep(0.01)
    laserHandle.startScan()                                         # Start waiting for the trigger  

    while dataReady.value == dataCheck.value:                       # Checks whether acquired data samples are ready for collection. WARNING, if timeout in picoScopeSetTrigger() is set to '0' this is a blocking statement!
        isReadyStatus = ps.ps4000aIsReady(chandle, ctypes.byref(dataReady))

    getValuseStatus = ps.ps4000aGetValues(chandle, 0, ctypes.byref(cmaxSamples), 0, 0, 0, ctypes.byref(overflow))
    assert_pico_ok(getValuseStatus)

    return np.ctypeslib.as_array(bufferAMax), np.ctypeslib.as_array(bufferBMax), np.ctypeslib.as_array(bufferCMax), timeIntervalns.value

# Function, calculates the number of samples acquired during the laser sweep. Compatible with series 4000 type PicoScopes.
# Input: PicoScope sampling rate, laser parameters.
# Return: number of samples (int)
def calcSampleNum(samplingRateDiv, laserParams):
    sampleNum = 0
    if(laserParams.DOMAIN == LASER_DOMAIN.WAVELENGTH):

        samplingRate = 8e7 / (1 + samplingRateDiv)        # See Programming Guide ps4000apg.en r5 page 17

        scanRange = laserParams.STOP - laserParams.START
        scanTime = float(scanRange) / float(laserParams.SCAN_SPEED)
        sampleNum = samplingRate * scanTime
    else:
        sampleNum = -1
        # TODO: in case the frequency domain is used put some code here
    return int(sampleNum)

def picoScopeConfigure(GlobalConfig):
    '''
    Configures the PicoScope.

    Arguments:
    ----------
    GlobalConfig - class containging global config parameters
    '''
    for i in range(0, 8):  # For PicoScope 4824 all unused channels have to be turned off!!!
        picoScopeSetChannel(i, 0, PICO_COUPLING.AC, PICO_RANGE.R500MV, 0)
    
    picoScopeSetChannel(GlobalConfig.picoParams.PICO_TRIGGER_CH, 1, PICO_COUPLING.DC, PICO_RANGE.R2V, 0) # Configure and enable the trigger channel
    picoScopeSetChannel(GlobalConfig.picoParams.PICO_MEAS_CH, 1, PICO_COUPLING.AC, PICO_RANGE.R200MV, 0) # Configure and enable the measurement interferometer channel, WARNING: range for this channel is overriden before every acquisition in fsiAcquireDistance()
    picoScopeSetChannel(GlobalConfig.picoParams.PICO_REF_CH, 1, PICO_COUPLING.AC, GlobalConfig.picoParams.PICO_REF_CH_RANGE, 0) # Configure and enable the reference interferometer channel
    picoScopeSetChannel(GlobalConfig.picoParams.PICO_GAS_CELL_CH, 1, PICO_COUPLING.AC, GlobalConfig.picoParams.PICO_GAS_CELL_CH_RANGE, 0) # Configure and enable the gas cell channel
    picoScopeSetTrigger(GlobalConfig.picoParams)

