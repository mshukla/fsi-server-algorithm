# FSI interferometer data processing functions - used to process data from the Fiber Bragg Sensors
# Functions used by the fbgDataAnalysis module
# Version: 1.0
# Author: J.Rutkowski
# Date: 12/2020
# CERN 2020

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from pyfsi.fsiEnums import *
import logging
from pyfsi.fsiExceptions import *
import time
import traceback



def dataCutout(wavelength, signal, minDist, maxDist):
    """
    Cut out the wavelength region of interest from the input data.

    Keyword arguments:
    ------------------
    wavelength    -- array with the wavelength data (from intensity)
    signal       -- array with the magnitude data (from intensity)
    minDist     -- minimum wavelength boundary
    maxDist     -- maximum wavelength boundary

    Return:
    -------
    wavelength[]  -- array containing the cutout wavelength values
    signal[]     -- array containing the magnitude values coresponding to the wavelength values
    """
    minIdx = -1
    maxIdx = -1
    for i in range(0, len(wavelength)):
        if minIdx == -1:
            if wavelength[i] >= minDist:
                minIdx = i

        if maxIdx == -1:
            if wavelength[i] >= maxDist:
                maxIdx = i
    
    indexesToCutOut = np.arange(int(minIdx), int(maxIdx))

    return wavelength[indexesToCutOut], signal[indexesToCutOut]


def fitFbgPeaks(wavelengthData, signalData, fbgParamsHandle, opMode, fullOutput=False):
    """
    Function which fits the interferometric peaks.
    Iterates over the currentDistences given in the fsiParams of the current channel.
    
    Keyword arguments:
    ------------------
    wavelengthData  -- array with the wavelength values (from FFT)
    signalData      -- array with the filtered signal intensity data (from FFT)
    fbgParamsHandle -- parameters of the measurement channel, FsiSignalParams-type
    opMode          -- setup operating mode SETUP_OP_MODE-type

    Return:
    -------
    if fullOutput == False: Peak wavelength values numpy array-type. Number of elements depends on the number of currentDistance values.
    if fullOutput == True: Outputs raw fit data
    warningMsg - if applicable
    """

    returnValues = np.array([])
    retStatus = {'Message':'OK', 'Problem':'', 'Center error': np.array([])} # Returned status message
    fitSignalData = {}
    fitWavelengthData = {}

    # If there are no initial values specified...
    if(fbgParamsHandle.currentDistance == []):
            retStatus['Message'] = 'No initial values specified for CH: ' + str(fbgParamsHandle.chanNumber)
            retStatus['Problem'] = 'No_Init_Val'
            returnValues = np.append(returnValues, -1)

    for peakIter in range(0, len(fbgParamsHandle.currentDistance)):

        maxWavelength = fbgParamsHandle.currentDistance[peakIter] + fbgParamsHandle.distanceWindow      # Maximum wavelength to target (nm) for the current peak
        minWavelength = fbgParamsHandle.currentDistance[peakIter] - fbgParamsHandle.distanceWindow      # Minimum wavelength to target (nm) for the current peak

        wavelengthCut, signalCut = dataCutout(wavelengthData, signalData, minWavelength, maxWavelength)        # Cut out the wavelength region of interest for peak identification

        try:
            signalPeak, _ = find_peaks(signalCut, distance=(len(signalCut)/4))
            returnValues = np.append(returnValues, wavelengthCut[signalPeak])
            retStatus['Center error'] = np.append(retStatus['Center error'], 0.0)
            
            if(fullOutput==True):
                fitSignalData[peakIter] = signalPeak
                fitWavelengthData[peakIter] = wavelengthCut[signalPeak]
        
        except Exception:
            returnValues = np.append(returnValues, wavelengthCut[np.argmax(signalCut)]) # If the fitting fails, return the distance corresponding to the highest sample.
            retStatus['Message'] = 'FBG fit failed for CH: ' + str(fbgParamsHandle.chanNumber) + '/ peak no: ' + str(peakIter) + traceback.format_exc()
            retStatus['Problem'] = 'Fbg_Fit_Fail'
            FsiExceptionCounter.fftFittingFailedExcept += 1
            logging.exception('FBG peak fitting error')

        if ((opMode == SETUP_OP_MODE.DEBUG_SHOW_PEAKS) or (opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_PEAKS) or (opMode == SETUP_OP_MODE.TEST_MODE)):
            print('Center: ' + str(wavelengthCut[signalPeak]) + ' nm')                               # Show report
            plt.plot(wavelengthCut[signalPeak], signalCut[signalPeak], 'x', label='data')               # Plot fit results
            plt.plot(wavelengthCut, signalCut, 'g-', label='data')
            plt.show()

        if ((opMode == SETUP_OP_MODE.DEBUG_SAVE_PEAKS) or (opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_PEAKS)):
            np.save('Debug\\' + str(time.time()) + '_PEAK_CH' + str(fbgParamsHandle.chanNumber) + '.npy', np.array([wavelengthCut, signalCut, wavelengthCut[signalPeak]]))

        if(opMode == SETUP_OP_MODE.DEBUG_SHOW_FFT):
            plt.plot(wavelengthData, signalData)
            plt.xlabel('Wavelength (nm)')
            plt.ylabel('Signal (a.u.)')
            plt.show()

        if(opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_FFT):
            np.save('Debug\\' + str(time.time()) + '_FFT_CH' + str(fbgParamsHandle.chanNumber) + '.npy', np.array([wavelengthData, signalData]))
            plt.plot(wavelengthData, signalData)
            plt.xlabel('Distance (m)')
            plt.ylabel('Signal (a.u.)')
            plt.show()
    
    if(fullOutput == True):
        return fitWavelengthData, fitSignalData, returnValues, retStatus
    else:
        return returnValues, retStatus