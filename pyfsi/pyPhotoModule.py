# Photodetection Module v1.0 interface functions.
# Author: J.Rutkowski
# Revision: 1.0
# CERN 11/2019
# Change log:
# - verion for Python 3.x

import serial
import time
from pyfsi.fsiEnums import *


def read_Line(serH):
    '''
    Reads a line from the serial port, the TLM-8700 laser module terminates each line with <CR><LF>
    Input: serH - handle to the laser's serial port
    Return: str -- read line
    '''
    str = ''
    while 1:
        ch = (serH.read()).decode()
        if(ch == '\r' or ch == ''):
            serH.read()                 # Reads and neglects the last <LF> from the serial buffer, otherwise it would get read during another call
            break
        str += ch
    return str


class photoModuleWrapper:
    # Opens setial connection with the photodetection module
    # Input: COM port address (string)
    # Returns: nothing
    def __init__(self, comPort):
        self.ser = serial.Serial(comPort, 9600, timeout=1)
        time.sleep(2)                                                      # The Arduino used in the Photodetection Module v1.0 is super slow to reply...


    # Closes the serial connection with the photodetection module
    # Input: none
    # Returns: nothing
    def disconnect(self):
        self.ser.close()


    # Verifies if the device is really the photodetection module
    # Input: none
    # Returns: Error status PHOTO_MODULE_ERR type
    def checkIdentity(self):
        ret = PHOTO_MODULE_ERR.COMM
        self.ser.write(str.encode('chState all off'))
        time.sleep(0.1)
        received = read_Line(self.ser).rstrip()
        if(received == 'All ch set to off'):
            ret = PHOTO_MODULE_ERR.OK
        return ret


    # Turns on or off selected channels
    # Input: channel number - PHOTO_MODULE_CHANNEL type; channel state - PHOTO_MODULE_CH_STATE type
    # Returns: Error status PHOTO_MODULE_ERR type
    def setChannelState(self, chNum, chState):
        ret = PHOTO_MODULE_ERR.COMM

        if (chNum != PHOTO_MODULE_CHANNEL.ALL):
            if (chState == PHOTO_MODULE_CH_STATE.ON):
                self.ser.write(str.encode('chState ' + str(chNum) + ' on'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'Ch ' + str(chNum) + ' set to on'):
                    ret = PHOTO_MODULE_ERR.OK
            elif (chState == PHOTO_MODULE_CH_STATE.OFF):
                self.ser.write(str.encode('chState ' + str(chNum) + ' off'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'Ch ' + str(chNum) + ' set to off'):
                    ret = PHOTO_MODULE_ERR.OK
        elif(chNum == PHOTO_MODULE_CHANNEL.ALL):
            if (chState == PHOTO_MODULE_CH_STATE.ON):
                self.ser.write(str.encode('chState all on'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'All ch set to on'):
                    ret = PHOTO_MODULE_ERR.OK
            elif (chState == PHOTO_MODULE_CH_STATE.OFF):
                self.ser.write(str.encode('chState all off'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'All ch set to off'):
                    ret = PHOTO_MODULE_ERR.OK
        return ret
    

    # Changes the gain of selected channels
    # Input: channel number - PHOTO_MODULE_CHANNEL type; channel gain - PHOTO_MODULE_CH_GAIN type
    # Returns: Error status PHOTO_MODULE_ERR type
    def setChannelGain(self, chNum, chGain):
        ret = PHOTO_MODULE_ERR.COMM

        if (chNum != PHOTO_MODULE_CHANNEL.ALL):
            if (chGain == PHOTO_MODULE_CH_GAIN.G5K):
                self.ser.write(str.encode('chGain ' + str(chNum) + ' 5'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'Ch ' + str(chNum) + ' gain set to 5'):
                    ret = PHOTO_MODULE_ERR.OK
            elif (chGain == PHOTO_MODULE_CH_GAIN.G20K):
                self.ser.write(str.encode('chGain ' + str(chNum) + ' 20'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'Ch ' + str(chNum) + ' gain set to 20'):
                    ret = PHOTO_MODULE_ERR.OK
        elif(chNum == PHOTO_MODULE_CHANNEL.ALL):
            if (chGain == PHOTO_MODULE_CH_GAIN.G5K):
                self.ser.write(str.encode('chGain all 5'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'All ch gains set to 5'):
                    ret = PHOTO_MODULE_ERR.OK
            elif (chGain == PHOTO_MODULE_CH_GAIN.G20K):
                self.ser.write(str.encode('chGain all 20'))
                time.sleep(0.1)
                received = read_Line(self.ser).rstrip()
                if(received == 'All ch gains set to 20'):
                    ret = PHOTO_MODULE_ERR.OK
        return ret
