# Sercalo 1x16 fiber optic switch RS-232 interface functions
# Author: J.Rutkowski
# Revision: 1.0
# CERN 11/2019
# Change log:
# - compatible with Python 3.x

import serial
import time
from pyfsi.fsiEnums import *


# Class containing the Sercalo 1x16 Fiber Optic Switch Module wrapper functions
class foSwitchWrapper:
    # Opens setial connection with the laser module 
    # Input: COM port address (string)
    # Returns: nothing
    def __init__(self, comPort):
        self.ser = serial.Serial(comPort, 9600, timeout=1)

    # Closes the serial connection with the FO Switch module
    # Input: none
    # Returns: nothing
    def disconnect(self):
        self.ser.close()

    # Verifies if the device is really the FO switch module
    # Input: none
    # Returns: Error status FO_SWITCH_ERR type
    def checkIdentity(self):
        ret = FO_SWITCH_ERR.COMM
        self.ser.write(str.encode('ID\r\n'))
        received = ((self.ser.readline()).decode()).split('|')
        if(received[0] == 'ID SC1x16'):
            ret = FO_SWITCH_ERR.OK
        return ret

    # Reads and returns the FO Switch module ID
    # Input: none
    # Returns: FO Switch module ID string
    def readId(self):
        self.ser.write(str.encode('ID\r\n'))
        received = (self.ser.readline()).decode()
        return received.rstrip("\n\r")

    # Reads and returns the FO Switch module internal temperature
    # Input: none
    # Returns: FO Switch module temperature (int)
    def readTemp(self):
        self.ser.write(str.encode('TMP\r\n'))
        received = (self.ser.readline()).decode()
        receivedSplit = received.split(' ')
        tempValue = receivedSplit[1].rstrip("\n\r") 
        return int(tempValue)

    # Sets the current FO Switch output channel
    # Input: Channel number (int) in the range of 1 to 16
    # Returns: Error status FO_SWITCH_ERR       
    def setChannel(self, chanNum):
        ret = FO_SWITCH_ERR.COMM
        self.ser.write(str.encode('SET ' + str(chanNum) + '\r\n'))
        received = (self.ser.readline()).decode()
        receivedSplit = received.split(' ')
        receivedChVal = int(receivedSplit[1].rstrip("\n\r"))
        if(receivedChVal == int(chanNum)):
            ret = FO_SWITCH_ERR.OK
        return ret