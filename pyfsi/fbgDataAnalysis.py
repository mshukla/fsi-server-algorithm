# FSI interferometer data analysis
# Description: Module used to analyse the data from Fiber Bragg Sensors
# Version: 1.1
# Author: J.Rutkowski
# Date: 11/2019
# CERN 2019

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import peakutils
from pyfsi.fsiFunctions import DataLinearize, filterDataButterworthLowpass, gasCellFindPeaks
from pyfsi.fbgFunctions import fitFbgPeaks
import ref_index
from pyfsi.fsiEnums import *
import time
from scipy.signal import savgol_filter, find_peaks
import logging
from pyfsi.fsiExceptions import *


def fbgDataAnalysis(rawDataStorage, fbgParamsHandle, GlobalConfig, envCond, fullOutput=False):
    '''
    Analyze the sweep data to get peak wavelength data. This function is used for data analysis of signals obtained with Fiber Bragg Sensos (FBG).
    It does not apply an FFT on the measurement interferometer signal, since for FBG sensors, the peak is in the intensity domain.
    Instead, it filters the intensity signal with a low-pass Butterworth filter.

    Keyword arguments:
    ------------------
    rawDataStorage  -- dictionary with the following keys:
                    ['Measurement int'] - measurement interferometer raw data
                    ['Gas cell']        - gas cell raw data
                    ['Reference int']   - reference interferometer raw data
                    ['Time interval']   - time interval between samples

    fbgParamsHandle -- channel parameters - object of class FsiSignalParams
    GlobalConfig    -- class containing the setup configuration
    envCond         -- dictionary containing the environmental conditions, eg. {'T': 20.0, 'H': 40.0, 'P': 96750} (deg C, %, Pa). Not used in FBG data analysis, left for compatibility.
    fullOutput      -- if True returns raw data, filtered signal intensity, fbg peak wavelength value(s) and gas cell identified peaks.

    Returns:
    --------
    if fullOutput==False: 
        Peak wavelength value(s), warningMsg  -- array with the distance values for the current channel. Length depends on the length of chanParams.currentDistance since muliple peaks can be detected for single channel.
    if fullOutput==True: 
        processedDataStorage, warningMsg
        processedDataStorage is a dictionary, containing the following keys:
            ['Filtered gas cell out']     - Processed and filtered gas cell ouptut values (np.array)
            ['Gas cell peak idxs']        - Indexes of the peaks identified in the gas cell spectrum (np.array)
            ['Sweep wavelength']          - x-axis wavelength values corresponding to each intensity sample in nm (np.array)
            ['Signal intensity']          - y-axis intensity values, filterred with a low-pass Butterworth filter (np.array)
            ['Fit value']                 - Peak wavelength value(s) in the intensity signal in nm
            ['Fit x-axis']                - Dictionary, containing (x-axis, wavelength) data of the identified peak wavelength in nm. One key is created for every wavelength present in fbgParamsHandle.currentDistance (Peak 1, Peak 2, ...etc)
            ['Fit y-axis']                - Dictionary, containing (y-axis, signal intensity) data of the identified peak wavelength. One key is created for every distance present in fbgParamsHandle.currentDistance (Peak 1, Peak 2, ...etc)
            ['Fit value']                 - Array containing peak wavelength values for each peak. This is considered as the wavelength (output) measured by the system! (np.array)
    '''
    retStatus = {}              # Returned status message
    processedDataStorage = {}   # Dictionary for processed data storage

    # CONSTANTS
    wavelength  = 1550.0    # Wavelength (nm)

    # DEBUG: Saving raw acquired data
    if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SAVE_RAW_DATA)):
        np.save('Debug\\' + str(time.time()) + '_RAW_DATA_CH' + str(fbgParamsHandle.chanNumber) + '.npy', np.array([np.float64(rawDataStorage['Reference int']), np.float64(rawDataStorage['Measurement int']), np.float64(rawDataStorage['Gas cell']), np.float64(rawDataStorage['Time interval'])]))
    
    rawDataStorage['Time interval'] = rawDataStorage['Time interval'] * 1e-9

    # DEBUG: Displays raw data
    if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SHOW_RAW_DATA) or (GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE)):
        print('Tinterval: ' + str(rawDataStorage['Time interval']) + ' s')
        fig = plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(rawDataStorage['Reference int'])
        plt.subplot(3, 1, 2)
        plt.plot(rawDataStorage['Measurement int'])
        plt.subplot(3, 1, 3)
        plt.plot(rawDataStorage['Gas cell'])
        plt.show()


    # DATA LINEARISATION and FBG peak fitting
    try:
        GAS_CELL_out, MEAS_out = DataLinearize(rawDataStorage['Time interval'], rawDataStorage['Reference int'], rawDataStorage['Gas cell'], rawDataStorage['Measurement int'])
        samplingFrequency = int(round(1/rawDataStorage['Time interval']))
        processedDataStorage['Signal intensity'] = filterDataButterworthLowpass(MEAS_out, 1e3, samplingFrequency)

    except Exception as e:
        logging.exception('Linearisation error: ' + str(e))
        raise FsiLinearizationException('Linearisation error: ' + str(e))

    if(GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE):
        plt.plot(MEAS_out)
        plt.show()


    # SWEEP SPEED CORRECTION USING GAS CELL DATA
    try:
        # Thorlabs photodetectors operate in photovoltaic mode so the signal is inversed
        if(GlobalConfig.photoModuleParams.VERSION == 'Thorlabs' and not ((GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE) and (GlobalConfig.externalRawData[-3:] == 'mat'))):
            GAS_CELL_out = GAS_CELL_out*(-1)
        
        processedDataStorage['Filtered gas cell out'] = savgol_filter(GAS_CELL_out, 101, 2, mode='mirror')  # Signal filtering - Savitzky-Golay filter
        processedDataStorage['Gas cell peak idxs'], retStatus['Gas cell'] = gasCellFindPeaks(processedDataStorage['Filtered gas cell out'], GlobalConfig.picoParams, GlobalConfig.laserParams, 'pyfsi\\SRM2519a.csv')  # Beta test

        GAS_CELL_wavelengths = np.genfromtxt('pyfsi\\SRM2519a.csv')                 # Load reference data for the gas cell used (NIST)
        GAS_CELL_wavelengths = GAS_CELL_wavelengths*1e-9                            # Convert to m
        
        alpha = np.polyfit(processedDataStorage['Gas cell peak idxs'], GAS_CELL_wavelengths,1)             # Calculate the real sweep speed (linear fit)
        alpha = alpha[0]

        startWavelength = GAS_CELL_wavelengths[0] - (alpha*processedDataStorage['Gas cell peak idxs'][0])
        stopWavelength = startWavelength + (len(rawDataStorage['Gas cell'])*alpha)
        processedDataStorage['Sweep wavelength'] = (np.linspace(startWavelength, stopWavelength, num=len(rawDataStorage['Gas cell'])))*1e9 # 1e9 to get values in nm
            
    except Exception as e:
        logging.exception('Gas cell analysis error' + str(e))
        raise FsiGasCellPeakSearchException('Gas cell fitting exception: ' + str(e))


    # Show gas cell fitting plots
    if ((GlobalConfig.setupOpMode == SETUP_OP_MODE.DEBUG_SHOW_GAS_CELL) or (GlobalConfig.setupOpMode == SETUP_OP_MODE.TEST_MODE)):
        plt.plot(GAS_CELL_out)
        plt.plot(processedDataStorage['Filtered gas cell out'])
        plt.plot(processedDataStorage['Gas cell peak idxs'], processedDataStorage['Filtered gas cell out'][processedDataStorage['Gas cell peak idxs']], 'x')
        plt.show()


    # Conditional return statements
    if(fullOutput == False): 
        # PEAK IDENTIFICATION - only wavelength value(s)
        processedDataStorage['Fit value'], retStatus['Fit'] = fitFbgPeaks(processedDataStorage['Sweep wavelength'], processedDataStorage['Signal intensity'], fbgParamsHandle, GlobalConfig.setupOpMode, fullOutput)
        return processedDataStorage['Fit value'], retStatus

    else:
        # PEAK IDENTIFICATION - full output
        processedDataStorage['Fit x-axis'], processedDataStorage['Fit y-axis'], processedDataStorage['Fit value'], retStatus['Fit'] = fitFbgPeaks(processedDataStorage['Sweep wavelength'], processedDataStorage['Signal intensity'], fbgParamsHandle, GlobalConfig.setupOpMode, fullOutput)
        return processedDataStorage, retStatus
