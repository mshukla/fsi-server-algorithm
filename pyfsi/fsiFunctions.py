# FSI interferometer data processing functions
# Functions used by the fsiDataAnalysis module
# Version: 1.1
# Author: J.Rutkowski
# Date: 11/2019
# CERN 2019

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert, butter, lfilter, find_peaks, savgol_filter
from scipy.interpolate import splev, splrep, InterpolatedUnivariateSpline
from pyfsi.fsiEnums import *
from lmfit.models import LorentzianModel
import matplotlib.pyplot as plt
import logging
from pyfsi.fsiExceptions import *
import time
import traceback


def simulatedTimestampCleanup(inTimestamp, figure):
    """ Workaround to supress doubbled values in t_simu, such values generate NaN's in splrep() - do not use this function! """
    execCtr = 0
    while(execCtr < (len(inTimestamp)-2)):
        inTimestamp.sort()
        for i in range(1, (len(inTimestamp)-1)):
            if (inTimestamp[i] == inTimestamp[i-1]):
                print('WARNING! Doubled values, please check the connection to and/or settings of the photodetection module!')
            else:
                execCtr += 1
    return inTimestamp


def DataLinearize(Tinterval, REF_IFM, GAS_CELL, MEAS) :
    """
    Data linearisation using the reference interferometer data

    Keyword arguments:
    ------------------
    Tinterval   -- sample spacing in seconds
    REF_IFM     -- reference interferometer raw data
    GAS_CELL    -- gas cell raw data
    MEAS        -- measurement interferometer raw data
    
    Return:
    -------
    GAS_CELL_out    -- linearized gas cell data
    MEAS_out        -- linearized measurement interferometer data
    """
    REF_IFM = filterDataButterworthHighpass(REF_IFM, 100000, (1/Tinterval))
    t = np.linspace(0.0, len(REF_IFM)*Tinterval, num=len(REF_IFM))   # Sample timestamps np array? TODO
    analytic_signal = hilbert(REF_IFM, axis=0)
    phase = np.angle(analytic_signal)
    instantaneous_phase = np.unwrap(phase, axis=0)
    del phase
    del analytic_signal

    print("TInv ", Tinterval)

    f_theor = max(instantaneous_phase)/(2*np.pi*(Tinterval*len(instantaneous_phase)))
    t_simu = instantaneous_phase/(2*np.pi*f_theor)
    t_simu[0] = 0                                            # Otherwise the first value of t is below the interpolation range
    t_simu=abs(t_simu)                                       # In case some of the points are negative. For spline interpolation data must increase monotonically.
    t_simu.sort()

#    print("T_Simu")
        
    MEAS_interpolated = splrep(t_simu, MEAS, k=1)            # Measurement interferometer signal interpolation
    MEAS_out = splev(t, MEAS_interpolated)
    del MEAS_interpolated

    print(len(MEAS_out))
    for k in MEAS_out:
        print("%.06f" %( k * 1e0 ) )

    GAS_CELL_interpolated = splrep(t_simu, GAS_CELL, k=1)    # Gas cell signal interpolation
    GAS_CELL_out = splev(t, GAS_CELL_interpolated)
    del GAS_CELL_interpolated

    return GAS_CELL_out, MEAS_out


def butter_lowpass(cutoff, fs, order):
    """
    Create a Butterworth lowpass filter, according to specified parameters.

    Keyword arguments:
    ------------------
    cutoff  -- cutoff frequency
    fs      -- sampling frequency
    order   -- filter order

    Return:
    -------
    Filter coefficients
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def filterDataButterworthLowpass(data, cutoff, fs):
    """
    Apply a Butterworth lowpass filter to the input data.

    Keyword aguments:
    -----------------
    data -- ata to be filterred
    cutoff -- cutoff frequency
    fs -- sampling frequency

    Return:
    -------
    Filtered data
    """
    b, a = butter_lowpass(cutoff, fs, order=2)  # Order shouldnt't be too big!
    ret = lfilter(b, a, data)
    return ret


def dataCutout(distance, power, minDist, maxDist):
    """
    Cut out the distance region of interest from the input data.

    Keyword arguments:
    ------------------
    distance    -- array with the distance data (from FFT)
    power       -- array with the magnitude data (from FFT)
    minDist     -- minimum distance boundary
    maxDist     -- maximum distance boundary

    Return:
    -------
    distance[]  -- array containing the cutout distance values
    power[]     -- array containing the magnitude values coresponding to the distance values
    """
    minIdx = -1
    maxIdx = -1
    for i in range(0, len(distance)):
        if minIdx == -1:
            if distance[i] >= minDist:
                minIdx = i

        if maxIdx == -1:
            if distance[i] >= maxDist:
                maxIdx = i
    
    indexesToCutOut = np.arange(int(minIdx), int(maxIdx))

    return distance[indexesToCutOut], power[indexesToCutOut]


def gasCellFindPeaks_old(filteredGasCellData, picoParams, laserParams):
    """
    First version of the gas cell peak identification algorithm. Do not use - depreciated!

    Keyword arguments:
    ------------------
    filteredGasCellData -- gas cell data after filtering
    picoParams          -- PicoScope setings
    laserParams         -- laser settings

    Return:
    -------
    Array with the detected peak positions
    """
    returnValue = []

    filteredGasCellData[0:40000] = 0                                 # TODO: prowizorka, to trzeba poprawic... Po filtrowaniu filtrem Butterwort'a na poczatku pojawia sie pik-artefakt
    maxPeak = np.max(filteredGasCellData)
    meanCell = np.mean(np.abs(filteredGasCellData))
    peakHeight = ((((maxPeak+meanCell)*0.0811)/0.5014) - meanCell) # This calculation is based on the nominal transmittance data from NIST

    MinIdx, _ = find_peaks(filteredGasCellData, prominence=1000, height=peakHeight, distance=1000)  # Peak identification in filtered measured gas cell signal

    iteration = 0
    while (MinIdx.size < 54):                                        # If the number of found peaks is too low, decrease the threshold and search again
        peakHeight = peakHeight - 50
        MinIdx, _ = find_peaks(filteredGasCellData, prominence=1000, height=peakHeight, distance=1000)
        iteration += 1
        if (iteration > 50):
            break
    
    if (iteration > 50):                                            # Protection from infinite loop TODO: niezbyt to wyrafinowane, dodac generacje exception
        print('Gas cell fitting error!')
    else:
        samplingRate = 0
        if(picoParams.PICO_SAMPLING_RATE == SAMPLING_RATE.R250MSPS):
            samplingRate = 1e9 / pow(2, picoParams.PICO_SAMPLING_RATE)        # See Programming Guide ps3000apg.en r14 page 8
        else:
            samplingRate = 125e6 / (picoParams.PICO_SAMPLING_RATE - 2) 

        branchDelta = ((1543.11423-1541.7528)/laserParams.SCAN_SPEED)*samplingRate  # Approximate number of samples between peak 0 from branch R and peak 1 from branch P
        branchDeltaMax = branchDelta + 0.1*branchDelta                              # 10% high-side error margin
        branchDeltaMin = branchDelta - 0.1*branchDelta                              # 10% low-side error margin

        peakZeroRBranchIdx = 0                                                      # Variable holding the index of peak 0 from R branch - initialized with 0

        for i in range(0, (len(MinIdx)-2)):
            idxDiff = MinIdx[i+1]-MinIdx[i]
            if((idxDiff >= branchDeltaMin) and (idxDiff <= branchDeltaMax) and (i >= 26)):
                peakZeroRBranchIdx = i

        if (peakZeroRBranchIdx != 0):
            returnValue = MinIdx[(peakZeroRBranchIdx-26) : ((peakZeroRBranchIdx-26)+48)]
        else:
            returnValue = [0.0]                                                        # TODO: Add an exception here!

    return returnValue


def butter_highpass(cutoff, fs, order):
    """
    Create a Butterworth Highpass filter, according to specified parameters.

    Keyword arguments:
    ------------------
    cutoff  -- cutoff frequency
    fs      -- sampling frequency
    order   -- filter order

    Return:
    -------
    Filter coefficients b and a.
    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a


def filterDataButterworthHighpass(data, cutoff, fs):
    """
    Apply a Butterworth highpass filter to the input data.

    Keyword arguments:
    ------------------
    data -- input data to be filtered
    cutoff -- cutoff frequency
    fs -- sampling frequency

    Return:
    -------
    Filtered data
    """
    b, a = butter_highpass(cutoff, fs, order=2)  # Order shouldnt't be too big!
    print("Butter ", (b), (a) )
    ret = lfilter(b, a, data)
    return ret


def butter_bandpass(lowCutoff, highCutoff, fs, order):
    """
    Create a Butterworth Bandpass filter, according to specified parameters.

    Keyword arguments:
    ------------------
    lowCutoff   -- low-side cutoff frequency
    highCutoff  -- high-side cutoff frequency
    fs          -- sampling frequency
    order       -- order of the filter

    Return:
    -------
    Filter coefficients b and a.
    """
    nyq = 0.5 * fs
    low = lowCutoff / nyq
    high = highCutoff / nyq
    b, a = butter(order, [low, high], btype='band', analog=False)
    return b, a


def filterDataButterworthBandpass(data, lowCutoff, highCutoff, fs):
    """
    Apply a Butterworth Bandpass filter to the input data.

    Keyword arguments:
    ------------------
    data        -- data to filter
    lowCutoff   -- low-side cutoff frequency
    highCutoff  -- high-side cutoff frequency
    fs          -- sampling frequency

    Return:
    -------
    Filtered data. 
    """
    b, a = butter_bandpass(lowCutoff, highCutoff, fs, order=2)  # Order shouldnt't be too big!
    ret = lfilter(b, a, data)
    return ret


def determineSamplingRate(picoParams):
    """
    Determines the PicoScope sampling rate based on the PICO_RANGE-type timebase.
    For reference on '3403D' model see PicoScope 3000a Series Programmer's Guide -  ps3000apg.en r14 - page 8
    For reference on '4824' model see PicoScope 4000a Series Programmer's Guide -  ps4000apg.en r5 - page 17

    Arguments:
    ------------------
    picoParams -- PicoScope configuratio class

    Return:
    -------
    Sampling rate in samples per second
    """
    samplingRate = 0
    if (picoParams.PICO_MODEL == '3403D'):
        if(picoParams.PICO_SAMPLING_RATE == SAMPLING_RATE.R250MSPS):
            samplingRate = 1e9 / pow(2, picoParams.PICO_SAMPLING_RATE)
        else:
            samplingRate = 125e6 / (picoParams.PICO_SAMPLING_RATE - 2)

    elif(picoParams.PICO_MODEL == '4824'):
        samplingRate = 8e7 / (1 + picoParams.PICO_SAMPLING_RATE)
    
    return samplingRate


def gasCellFittingValidator(errorData):
    '''
    Validating function used to verify the gas cell fitting algorithm. The laser sweep speed is not uniform throughout the sweep
    therefore some fitting errors may occur during the wavelength correction. If this function returns 'False' while the manual 
    verification shows that the fitting is correct increase the validationThreshold.

    Argument:
    ---------
    errorData - differences between the theoretical peak occurence times and occurence times determined by the fitting algorithm

    Return:
    -------
    ret - True gas cell fitting correct, False - measurement data may not be reliable, check the fitting manually
    '''
    validationThreshold = 1e4
    ret = True
    for i in range(0, len(errorData)-2):
        if((np.abs(errorData[i]-errorData[i+1])) > validationThreshold):
            ret = False
    return ret


def gasCellFindPeaks(filteredGasCellData, picoParams, laserParams, wavelengthListPath):
    """
    New peak identifying algorithm for the HCN gas cell fitting. Supposed o be more robust and noise immune.
    Use this instead of gasCellFindPeaksBeta_old
    First, it calulates the theoretical peak positions, based on the NIST wavelength data
    Later, identifies the delay of the sweep start, which for the TLM-8700 laser is usually around ~200 us, and applies a correction to the theoretical peak positions
    Finally it windows the signal around each theoretical peak position and finds the signal maximum.

    Keyword arguments:
    ------------------
    filteredGasCellData -- filtered gas cell signal
    picoParams          -- PicoScope settings, PicoScopeInitParams-type
    laserParams         -- laser settings, LaserInitParams-type
    wavelengthListPath  -- path to the NIST data for the HCN cell (SRM2519a.csv)
    
    Return:
    -------
    An array with identified peak positions (indexes) and warning message if applicable
    """
    nonlinearityCorrectionFactor = 1.5 # Used to account for the nonlinearity of laser sweep speed - empirically determined value!
    peaksFoundIdxs = np.array([])
    wavelengthList = np.genfromtxt(wavelengthListPath)
    samplingRate = determineSamplingRate(picoParams)
    theorPeakIdxs = ((wavelengthList-laserParams.START)/laserParams.SCAN_SPEED)*samplingRate # Calculation of theoretical peaks positions
    initialFitProminence = np.abs(np.min(filteredGasCellData)-np.max(filteredGasCellData))/10 # Prominence for initial fitting depands on the signal level
    retStatus = {'Message':'OK', 'Problem':''} # Returned status information

    # Defining the sweep start delay
    maxFilteredGasCellData = np.max(filteredGasCellData)
    minFilteredGasCellData = np.min(filteredGasCellData)
    signalTreshold = (minFilteredGasCellData + 0.33*(np.abs(minFilteredGasCellData) + np.abs(maxFilteredGasCellData)))
    deltaPeakIdxs = theorPeakIdxs[27] - theorPeakIdxs[26]   # Theoretical distance between peak 0 of R branch an peak 1 of P branch
    maxDeltaPeakIdxs = deltaPeakIdxs + 0.01*deltaPeakIdxs   # Upper distance margin for peak search
    minDeltaPeakIdxs = deltaPeakIdxs - 0.01*deltaPeakIdxs   # Lower distance margin for peak search

    initialPeakIdxs, _ = find_peaks(filteredGasCellData, prominence=initialFitProminence, height=signalTreshold, distance=1000) # Initial peak search - supposed to find peaks between the two absorption branches

    # Looking for peak 0 of R branch and peak 1 of P branch
    wavelengthCorrection = 0
    for i in range(0, len(initialPeakIdxs)-1):
        for j in range((i+1), len(initialPeakIdxs)):
            currentdeltaPeakIdxs = initialPeakIdxs[j] - initialPeakIdxs[i]
            if((currentdeltaPeakIdxs >= minDeltaPeakIdxs) and (currentdeltaPeakIdxs <= maxDeltaPeakIdxs)):
                wavelengthCorrection = initialPeakIdxs[i] - theorPeakIdxs[26] # Correction of wavelength corresponding to the delay of sweep start

    theorPeakIdxs = theorPeakIdxs + wavelengthCorrection/nonlinearityCorrectionFactor    # Application of the correction to the theoretical peak positions. The nonlinearityCorrectionFactor accounts for the nonlinearity of the laser sweep speed, uncomment the debug at the end of the gasCellFindPeaks() function for more info.

    # Windowing of the signal around each corrected theoreticalpeak position and search for maximum values
    for i in range(0, len(theorPeakIdxs)):
        if (i == 0):    # First peak
            rightDiff = int((theorPeakIdxs[i+1] - theorPeakIdxs[i])/2)
            leftDiff  = rightDiff
            signalWindow = filteredGasCellData[int(theorPeakIdxs[i]-leftDiff):int(theorPeakIdxs[i]+rightDiff)]
            peaksFoundIdxs = np.append(peaksFoundIdxs, (np.argmax(signalWindow) + (theorPeakIdxs[i]-leftDiff)))
        
        elif (i == (len(theorPeakIdxs)-1)): # Last peak
            leftDiff  = int((theorPeakIdxs[i] - theorPeakIdxs[i-1])/2)
            rightDiff = leftDiff
            signalWindow = filteredGasCellData[int(theorPeakIdxs[i]-leftDiff):int(theorPeakIdxs[i]+rightDiff)]
            peaksFoundIdxs = np.append(peaksFoundIdxs, (np.argmax(signalWindow) + (theorPeakIdxs[i]-leftDiff)))
        
        else: # All the other peaks
            rightDiff = int((theorPeakIdxs[i+1] - theorPeakIdxs[i])/2)
            leftDiff  = int((theorPeakIdxs[i] - theorPeakIdxs[i-1])/2)
            signalWindow = filteredGasCellData[int(theorPeakIdxs[i]-leftDiff):int(theorPeakIdxs[i]+rightDiff)]
            peaksFoundIdxs = np.append(peaksFoundIdxs, (np.argmax(signalWindow) + (theorPeakIdxs[i]-leftDiff)))

        peaksFoundIdxs = peaksFoundIdxs.astype('int64') # Convert the peak positions to int type, since the positions are acually defined in integer sample numbers
    
    if (gasCellFittingValidator(theorPeakIdxs-peaksFoundIdxs) == False): # Checks the correctness of gas cell peak fitting
        logging.warning('Gas cell peak fitting error exceeds threshold. Measured value may not be precise')
        retStatus['Message'] = 'Warning! Measured value may not be precise. Gas cell peak fitting error exceeds threshold. Please verify the fitting manually and increase the nonlinearityCorrectionFactor or the validationThreshold.; '
        retStatus['Problem'] = 'Gas_Cell_Wrn'
    #plt.plot(theorPeakIdxs-peaksFoundIdxs) # Uncomment to debug the gas cell fitting error
    #plt.show()
    return peaksFoundIdxs, retStatus


def fftFitValid(fitResult):
    '''
    Validates whether the fit result is within the acceptable error range
    
    Arguments:
    ----------
    fitResult - output of the lmfit fitting function

    Return:
    -------
    True if ok, False if error is too big
    '''
    ret = True

    if(isinstance(fitResult.params['center'].stderr, (int, float)) and isinstance(fitResult.params['amplitude'].stderr, (int, float))):

        fftFitCenterErrorThreshold = 20e-6     # Error on the frequency (distance) value in m
        fftFitAmplitudeErrorThreshold = 100    # Error on the amplitude of the peak in % - determined emiprically

        if (fitResult.params['center'].stderr > fftFitCenterErrorThreshold): # Frequency (distance)
            ret = False
            print('Center error: ' + str(fitResult.params['center'].stderr))

        if ((fitResult.params['amplitude'].stderr/fitResult.params['amplitude'].value)*100 > fftFitAmplitudeErrorThreshold): # Amplitude
            ret = False
            print('Amplitude error: ' + str(fitResult.params['amplitude'].stderr))

        #print(str((fitResult.params['center'].stderr/fitResult.params['center'].value)*100))
        #print(str((fitResult.params['amplitude'].stderr/fitResult.params['amplitude'].value)*100))

    return ret


def fitFsiPeaks(distanceData, powerData, fsiParamsHandle, opMode, fullOutput=False):
    """
    Function which fits the interferometric peaks.
    Iterates over the currentDistences given in the fsiParams of the current channel.
    
    Keyword arguments:
    ------------------
    distanceData    -- array with the distance values (from FFT)
    powerData       -- array with the magnitude data (from FFT)
    fsiParamsHandle -- parameters of the measurement channel, FsiSignalParams-type
    opMode          -- setup operating mode SETUP_OP_MODE-type

    Return:
    -------
    if fullOutput == False: Peak center values numpy array-type. Number of elements depends on the number of currentDistance values. Returns '0.0' in case the fitting function fails.
    if fullOutput == True: Outputs raw fit data
    warningMsg - if applicable
    """
    returnValues = np.array([])
    retStatus = {'Message':'OK', 'Problem':'', 'Center error': np.array([])} # Returned status message
    fitMagnitudeData = {}
    fitDistanceData = {}

    # If there are no initial values specified...
    if(fsiParamsHandle.currentDistance == []):
            retStatus['Message'] = 'No initial values specified for CH: ' + str(fsiParamsHandle.chanNumber)
            retStatus['Problem'] = 'No_Init_Val'
            returnValues = np.append(returnValues, -1)

    for peakIter in range(0, len(fsiParamsHandle.currentDistance)):

        maxDistance = fsiParamsHandle.currentDistance[peakIter] + fsiParamsHandle.distanceWindow      # Maximum distance to target (m) for the current peak
        minDistance = fsiParamsHandle.currentDistance[peakIter] - fsiParamsHandle.distanceWindow      # Minimum distance to target (m) for the current peak

        dstCut, powerCut = dataCutout(distanceData, powerData, minDistance, maxDistance)        # Cut out the distance region of interest for fitting
        fitModel_Lorentzian = LorentzianModel(nan_policy='omit')                                # Fit type - Lorentzian, don't generate exceptions in case of NaN values

        initialGuess = fitModel_Lorentzian.guess(powerCut, x=dstCut)   # Initial guess of the fitted values for the Lorentzian distribution (doesn't work on composite models...)
        initialGuess['amplitude'].min   = 0.0
        initialGuess['sigma'].min       = 0.0
        initialGuess['center'].min      = minDistance
        initialGuess['center'].max      = maxDistance

        try:
            fitOut  = fitModel_Lorentzian.fit(powerCut, initialGuess, x=dstCut)   # Start fitting, fo debug use arg: "iter_cb=fittingDebug" with the fittingDebug() funtion enabled 
            returnValues = np.append(returnValues, fitOut.params['center'].value)
            retStatus['Center error'] = np.append(retStatus['Center error'], fitOut.params['center'].stderr)
            
            if(fullOutput==True):
                fitMagnitudeData[peakIter] = fitOut.best_fit
                fitDistanceData[peakIter] = dstCut

        except Exception:
            returnValues = np.append(returnValues, dstCut[np.argmax(powerCut)]) # If the fitting fails, return the distance corresponding to the highest sample.
            retStatus['Message'] = 'FFT fit failed for CH: ' + str(fsiParamsHandle.chanNumber) + '/ peak no: ' + str(peakIter) + traceback.format_exc()
            retStatus['Problem'] = 'Fft_Fit_Fail'
            FsiExceptionCounter.fftFittingFailedExcept += 1
            logging.exception('FFT peak fitting error')
        
        if ((opMode == SETUP_OP_MODE.DEBUG_SHOW_PEAKS) or (opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_PEAKS) or (opMode == SETUP_OP_MODE.TEST_MODE)):
            print(fitOut.fit_report(min_correl=0.25))                               # Show report
            plt.semilogy(dstCut, fitOut.best_fit, 'b-', label='data')               # Plot fit results
            plt.semilogy(dstCut, powerCut, 'g-', label='data')
            plt.show()

        if ((opMode == SETUP_OP_MODE.DEBUG_SAVE_PEAKS) or (opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_PEAKS)):
            np.save('Debug\\' + str(time.time()) + '_PEAK_CH' + str(fsiParamsHandle.chanNumber) + '.npy', np.array([dstCut, fitOut.best_fit, powerCut]))

        if(opMode == SETUP_OP_MODE.DEBUG_SHOW_FFT):
            plt.semilogy(distanceData, powerData)
            plt.xlabel('Distance (m)')
            plt.ylabel('Signal (a.u.)')
            plt.show()

        if(opMode == SETUP_OP_MODE.DEBUG_SHOW_SAVE_FFT):
            np.save('Debug\\' + str(time.time()) + '_FFT_CH' + str(fsiParamsHandle.chanNumber) + '.npy', np.array([distanceData, powerData]))
            plt.semilogy(distanceData, powerData)
            plt.xlabel('Distance (m)')
            plt.ylabel('Signal (a.u.)')
            plt.show()
    
    if(fullOutput == True):
        return fitDistanceData, fitMagnitudeData, returnValues, retStatus
    else:
        return returnValues, retStatus
