# Keopsys CEFA EDFA amplifier interface functions.
# Author: J.Rutkowski
# Revision: 1.0
# CERN 11/2019
# Change log:
# - version compatible with Python 3.x

import serial
import time
from pyfsi.fsiEnums import *


def read_Line(serH):
    """
    Reads a line from the serial port, the Keopsys CEFA EDFA amplifier module terminates each line with <CR>
    Input: serH -- handle to the laser's serial port
    Returns: retStr -- read line
    """
    retStr = ''
    while 1:
        ch = (serH.read()).decode()
        if(ch == '\r' or ch == ''):
            break
        retStr += ch
    return retStr


class keopsysWrapper:
    # Opens serial connection with the amplifier
    # Input: COM port address (string)
    # Returns: nothing
    def __init__(self, comPort):
        self.ser = serial.Serial(comPort, 19200, timeout=1)


    # Closes the serial connection with the amplifier module
    # Input: none
    # Returns: nothing
    def disconnect(self):
        self.ser.close()


    # Verifies if the device is really the Keopsys EDFA module
    # Input: none
    # Returns: Error status EDFA_ERR type
    def checkIdentity(self):
        ret = EDFA_ERR.COMM
        self.ser.write(str.encode('SNU?\r'))
        received = read_Line(self.ser).split('=')
        if(received[0] == 'SNU'):
            ret = EDFA_ERR.OK
        return ret
    
    
    # Reads and returns the amplifier serial number
    # Input: none
    # Returns: amplifier serial number (string)
    def readId(self):
        self.ser.write(str.encode('SNU?\r'))
        received = read_Line(self.ser)
        return received

    
    # Turns the amplifier on or off by swiching the control mode (asservisemsnt) 0 - off, 2 - on. WARNING: even though the amplifier is off there may be a beam at the output. Always check the input power too!
    # Input: EDFA state - EDFA_STATE type
    # Returns: Error status EDFA_ERR type
    def setEdfaState(self, eState):
        ret = EDFA_ERR.COMM
        self.ser.write(str.encode('ASS=' + str(eState) + '\r'))
        received = read_Line(self.ser)
        if((eState == EDFA_STATE.ON) and (received == 'ASS!2')):
            ret = EDFA_ERR.OK
        elif((eState == EDFA_STATE.OFF) and (received == 'ASS!0')):
            ret = EDFA_ERR.OK
        return ret


    # Sets the amplifier output power
    # Input: value of power in dBm, min: 10.0, max: 18.0 (float)
    # Returns: Error status EDFA_ERR type
    def setEdfaOutputPower(self, aPower):
        ret = EDFA_ERR.COMM
        if '.' not in str(aPower):                  # The value has to be in the for e.g. '12.0'
            aPower = str(aPower) + '.0'
        elif (len(str(aPower).split('.')) < 2):     # In case there is only a dot and nothing following it
            aPower = str(aPower) + '0'

        self.ser.write(str.encode('CPU=' + str(aPower) + '\r'))
        received = read_Line(self.ser)
        if(received == 'CPU!'):
            ret = EDFA_ERR.OK
        return ret

    
    # Reads and returns the amplifier output power
    # Input: none
    # Returns: amplifier output power in dBm (string)
    def readOutputPower(self):
        self.ser.write(str.encode('PUS?\r'))
        received = read_Line(self.ser)
        return received

    
    # Reads and returns the amplifier input power
    # Input: none
    # Returns: amplifier input power in dBm (string)
    def readInputPower(self):
        self.ser.write(str.encode('PUE?\r'))
        received = read_Line(self.ser)
        return received

    
    def setCheckOutputPower(self, aPower, pThreshold=0.15):
        ''' Sets the output power and checks ten times the actual value
        Input: aPower: value of power to set (dBm); pThreshold - acceptance threshold of power value (usualy the output bower is around 0.1 dBm lower than setpoint)
        '''
        ret = EDFA_ERR.COMM
        if(self.setEdfaOutputPower(aPower) == EDFA_ERR.OK):
            for i in range(100):
                outputVal = float(self.readOutputPower().split('=')[1])
                time.sleep(0.1)
                if(((float(aPower)-outputVal) < 0.15) and ((outputVal-float(aPower)) < 0.15)):
                    ret = EDFA_ERR.OK
                    break
        return ret