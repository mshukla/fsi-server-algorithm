# Module containing the functions used to detect the COM ports of devices used by the FSI setup.
# Author: J.Rutkowski
# Revision: 1.0
# CERN 11/2019
# Change log:
# - version compatible with Python 3.x


import serial.tools.list_ports
from pyfsi.fsiEnums import *
from  pyfsi.fiberOpticSwitch import *
from pyfsi.pyKeopsys import *
from pyfsi.TLM8700 import *
from pyfsi.pyPhotoModule import *
import time


'''
Function which looks for the COM ports of used devices. For FSI-specific devices (Laser, EDFA, fiber optic switch and photodetedtion module)
it also verifies the connection using the checkIdentity method implemented in the communication wrappers.
For other devices it checks the VID, PID, Serial number, manufacturer and the description.
Input: Dictionary with the devices names (local) and serial number (for FSI-specific) or VID, PID, Serial number, manufacturer or description
Output: Dictionary with the devices names (the same as input) and COM port reference
Example usage: detectedDevices = determineComPorts({'TLM8700': '8', 'EDFA': 'FT2T83SXA', 'PhotoModule': '75735303931351716140', 'FOSwitch': 'FT9LE7ZGA', 'Wyler': 'Moxa Inc.'})
'''
def determineComPorts(devicesDict):
    detectedDevices = {}
    comlist = serial.tools.list_ports.comports()
    
    for comDevice in comlist:
        for deviceName, deviceSn in devicesDict.items():
            if((comDevice.serial_number == deviceSn) and deviceName == 'TLM8700'):
                laser = Tlm8700Wrapper(comDevice.device)
                if (laser.checkIdentity() == LASER_ERR.OK):
                    detectedDevices.update({'TLM8700': comDevice.device})
                    print('TLM8700 laser module found on ' + str(comDevice.device))
                else:
                    raise IOError('TLM8700 laser module connection problem on ' + str(comDevice.device))
                laser.disconnect()
            
            elif((comDevice.serial_number == deviceSn) and deviceName == 'PhotoModule'):
                photoModule = photoModuleWrapper(comDevice.device)
                if (photoModule.checkIdentity() == PHOTO_MODULE_ERR.OK):
                    detectedDevices.update({'PhotoModule': comDevice.device})
                    print('Photodetection module found on ' + str(comDevice.device))
                else:
                    raise IOError('Photodetection module connection problem on ' + str(comDevice.device))
                photoModule.disconnect()

            elif((comDevice.serial_number == deviceSn) and deviceName == 'EDFA'):
                keopsys = keopsysWrapper(comDevice.device)
                if(keopsys.checkIdentity() == EDFA_ERR.OK):
                    detectedDevices.update({'EDFA': comDevice.device})
                    print('EDFA amplifier found on ' + str(comDevice.device))
                else:
                    raise IOError('EDFA amplifier problem on ' + str(comDevice.device))
                keopsys.disconnect()

            elif((comDevice.serial_number == deviceSn) and deviceName == 'FOSwitch'):
                foSwitch = foSwitchWrapper(comDevice.device)
                if(foSwitch.checkIdentity() == FO_SWITCH_ERR.OK):
                    detectedDevices.update({'FOSwitch': comDevice.device})
                    print('Fiber optic switch found on ' + str(comDevice.device))
                else:
                    raise IOError('FO switch problem on ' + str(comDevice.device))
                foSwitch.disconnect()

            elif((comDevice.serial_number == deviceSn) or \
            (comDevice.manufacturer == deviceSn) or \
            (comDevice.vid == deviceSn) or \
            (comDevice.pid == deviceSn) or \
            (comDevice.description == deviceSn)):
                detectedDevices.update({deviceName: comDevice.device})
                print('Device ' + str(deviceName) + ' found on ' + str(comDevice.device))


    for deviceName in devicesDict.keys():
        if deviceName not in detectedDevices:
            raise IOError('Device ' + str(deviceName) + ' not found!')

    return detectedDevices
