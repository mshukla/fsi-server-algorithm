# FSI interferometer additional wrapper functions
# Functions used by the Main Program
# Version: 1.2
# Author: J.Rutkowski
# Date: 06/2020
# CERN 2020

from pyfsi.fsiEnums import *
from pyfsi.TLM8700 import *
from pyfsi.fiberOpticSwitch import *
import matplotlib.pyplot as plt
from pyfsi.fsiDataAnalysis import *
from pyfsi.fbgDataAnalysis import *
import time
from shutil import copy
from pyfsi.fsiExceptions import *
import logging


def saveToFile(value, fileName):
    """
    Saves data to file. Appends and "newlines" at every call. If the file doesn't exist it creates it.

    Keyword arguments:
    ------------------
    value -- data string to save
    fileName -- filename and path (string!)

    Return:
    -------
    Nothing
    """
    file = open(fileName, 'a+')
    file.write(str(value) + '\n')
    file.close()

def initializePicoScope(GlobalConfig):
    '''
    Initializes and configures the PicoScope. Switches between different models ('4824' or '3403D') and calls the appropriate wrapper library.

    Arguments:
    ----------
    GlobalConfig - class containging global parameters

    Return:
    -------
    daq - handle to the picoscope wrapper
    '''
    ret = 0
    if(GlobalConfig.picoParams.PICO_MODEL == '4824'):
        import pyfsi.dataAcquisitionPs4000a as daq
        GlobalConfig.picoParams.PICO_NUMBER_OF_SAMPLES = daq.calcSampleNum(GlobalConfig.picoParams.PICO_SAMPLING_RATE, GlobalConfig.laserParams)
        daq.picoScopeOpen()
        ret = daq

    elif(GlobalConfig.picoParams.PICO_MODEL == '3403D'):
        import pyfsi.dataAcquisitionPs3000a as daq
        GlobalConfig.picoParams.PICO_NUMBER_OF_SAMPLES = daq.calcSampleNum(GlobalConfig.picoParams.PICO_SAMPLING_RATE, GlobalConfig.laserParams)
        daq.picoScopeOpen()
        ret = daq

    return ret

def fsiAcquireDistance(picoHandle, chanParams, GlobalConfig, laserHandle, foHandle, envCond={}):
    """
    Realizes the data acquisition and calculates distance.

    Keyword arguments:
    ------------------
    picoHandle          -- handle to the PicoScope wrapper
    chanParams          -- channel parameters - object of class FsiSignalParams
    GlobalConfig        -- configuration parameters
    laserHandle         -- handle to the laser serial port instance
    foHandle            -- handle to the fiber optic switch serial port instance
    envCond             -- dictionary containing the environmental conditions, eg. {'T': 20.0, 'H': 40.0, 'P': 96750} (deg C, %, Pa). If empty or incomplete the distance is calculated for a refractive index = 0.

    Return:
    -------
    measResult  -- array with the distance values for the current channel. Length depends on the length of chanParams.currentDistance since muliple peaks can be detected for single channel
    """
    if(GlobalConfig.setupOpMode != SETUP_OP_MODE.TEST_MODE):    
        foHandle.setChannel(chanParams.chanNumber)
        time.sleep(0.2)  #TODO: switching time between extreme fiber optic switch channels, needs revising to optimise execution time... eg. switch to the next channel after acquisition, before distance calculation.
        picoHandle.picoScopeSetChannel(GlobalConfig.picoParams.PICO_MEAS_CH, 1, PICO_COUPLING.AC, chanParams.picoRange, 0)    
        measInt, gasCell, refInt, timeInterval = picoHandle.acquireData(laserHandle, GlobalConfig.picoParams)  # Acquire data samples

        
        rawDataStorage = {
            'Measurement int'   : measInt, # Raw data from the measurement interferometer
            'Gas cell'          : gasCell, # Raw data from the gas cell
            'Reference int'     : refInt, # Raw data from the reference interferometer
            'Time interval'     : timeInterval           # Sampling period
        }
        
        try:
            if(chanParams.fbg == False):
                measResult, warningMsg = fsiDataAnalysis(rawDataStorage, chanParams, GlobalConfig, envCond)  # Calculate distances from the acquired data
            else:
                measResult, warningMsg = fbgDataAnalysis(rawDataStorage, chanParams, GlobalConfig, envCond)  # Calculate distances from the acquired data

        except FsiLinearizationException as e:
            FsiExceptionCounter.linearizationExcept += 1
            measResult = np.full((len(chanParams.currentDistance),), -1.0) # Returns a np array of length equal to the number of searched peaks, filled with -1.0's
            print(e)

        except FsiGasCellPeakSearchException as e:
            FsiExceptionCounter.gasCellPeakSearchExcept += 1
            measResult = np.full((len(chanParams.currentDistance),), -1.0) # Returns a np array of length equal to the number of searched peaks, filled with -1.0's
            print(e)

        except FsiMeasurementIntFftCalcException as e:
            FsiExceptionCounter.fftCalcFailedExcept += 1
            measResult = np.full((len(chanParams.currentDistance),), -1.0) # Returns a np array of length equal to the number of searched peaks, filled with -1.0's
            print(e)


    else:
        # External data in te form of numpy binary file
        if(GlobalConfig.externalRawData[-3:] == 'npy'):
            externalData = np.load(GlobalConfig.externalRawData, allow_pickle=True)
            refInt          = externalData[0]
            measInt         = externalData[1]
            gasCell         = externalData[2]
            timeInterval    = externalData[3]
        
        # External data in the form of a mat file, saved with the LabView program
        elif(GlobalConfig.externalRawData[-3:] == 'mat'):
            import scipy.io as sio
            externalData = sio.loadmat(GlobalConfig.externalRawData)                    # Import data from .mat file
            refInt = np.asarray(externalData['REF_INTERFERO']).squeeze()    # Reference interferometer signal
            measInt = np.asarray(externalData['INTERFEROMETER']).squeeze()  # Measurement interferometer signal
            gasCell = -1*np.asarray(externalData['GAS_CELL']).squeeze()        # Gas cell signal
            timeInterval = np.asarray(externalData['T_INTERVAL']).squeeze() # Sample spacing
            print('Time interval: ' + str(timeInterval))
        
        else:
            print('Error: Unknown external file type')
        
        measResult, analysisStatus = fsiDataAnalysis(refInt, measInt, gasCell, timeInterval, chanParams, GlobalConfig, envCond)  # Calculate distances, using the external Data. WARNING! Pico and laser settings have to be the same as the ones used to acquire the external data!!!
        
        if(analysisStatus['Gas cell']['Message'] != ''): print(analysisStatus['Gas cell']['Message'])
        if(analysisStatus['Fft fit']['Message'] != ''): print(analysisStatus['Fft fit']['Message'])

    # This part checks the correctness of the results, if discrepancies are found between this value and the previous one, the fitting window is not displaced.
    moveFittingWindow = True
    for peakIteration in range(0, len(chanParams.currentDistance)): # Iterate over detected peaks
        
        # If values between two subsequent measurements exceeds 1 mm report a warning and do not save the value as initial for next measurement (do not move the fitting window)
        if((abs(chanParams.currentDistance[peakIteration] - measResult[peakIteration]) > chanParams.distanceWindow) or (abs(chanParams.currentDistance[peakIteration] - measResult[peakIteration]) < -chanParams.distanceWindow)):
            FsiExceptionCounter.largeSampleJumpWarning += 1
            logging.warning('Sample to sample spacing exceeds limit, CH' + str(chanParams.chanNumber) + ', peak: ' + str(peakIteration+1) + ',' + str(chanParams.currentDistance[peakIteration]) + ',' + str(measResult[peakIteration]))
            print('Warning! Sample to sample spacing exceeds limit on CH ' + str(chanParams.chanNumber) + ' peak no: ' + str(peakIteration+1))
            moveFittingWindow = False

        # Check if the peaks do not overlap. If values for two subsequent peaks are within 50 um report a warning and do not save the value as initial for next measurement (do not move the fitting window)
        if (peakIteration != (len(chanParams.currentDistance)-1)):
            peakDiff = abs(measResult[peakIteration] - measResult[peakIteration+1]) 
            if(peakDiff < 50e-6):
                FsiExceptionCounter.fsiPeaksOverlapingWarning += 1
                logging.warning('Overlapping peaks, CH' + str(chanParams.chanNumber) + ', peaks: ' + str(peakIteration+1) + ',' + str(peakIteration+2))
                print('Warning! Sample to sample spacing exceeds limit on CH ' + str(chanParams.chanNumber) + ', peaks: ' + str(peakIteration+1) + ',' + str(peakIteration+2))
                moveFittingWindow = False
    
        # Otherwise move the fitting window for the next measurement
        if(moveFittingWindow == True):
            chanParams.currentDistance[peakIteration] = measResult[peakIteration]
    
    return measResult

def copyResults(sourceFilePath, destFolder):
    """
    Copies the results file to a network destination folder for backup. Used to store local data on DFS

    Keyword arguments:
    ------------------
    sourceFilePath  -- local file path
    destFolder      -- network destination path

    Return:
    -------
    Nothing if ok. If copying encounters problems prints an error. Shouldn't block program execution.
    """
    try:
        copy(sourceFilePath, destFolder)
        print('Copied!')
    except:
        print('Copy error')
