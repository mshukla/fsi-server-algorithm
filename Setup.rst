
Install
---------

*******************************
Prepare environment
*******************************
1] Stop Nouveau from loading.  modprobe blacklist  nouveau driver.
   either use blacklist or add to grub cmdline
   Or use script - nouveau_stop.sh
    
2] Removed using sudo rmmod and sudo modprobe -r 

3] Permanent blacklist with grub menu: 
    `vi   /etc/default/grub`
    Add GRUB_CMDLINE_LINUX=     `rdblacklist=nouveau nouveau.modeset=0`
    `grub2-mkconfig  -o /boot/grub2/grub.cfg`
    
   force a new kernel to run.

   blacklist in modprobe.d -> nvidia-installer-nouveau.conf
   `blacklist nouveau
    options nouveau  modset =0`

  4] Install kernel-devel and gcc
     `sudo yum install gcc kernel-devel`
  
 5] Check if system is in text mode.
    `systemctl set-default multi-user.target`
    `systemctl get-default`
     Reboot in text mode, but we are already so not required

6] ssh into server

7] Go into directory where you have .run 
   `sh ./NVIDIA-Linux-x86_64-460.84.run`

8] `lsmod|grep nvidia` -To check installed drivers and dmesg



**************************************
Pre-requisites if X-Server is running
**************************************

kill your current X server session by typing 
`sudo service lightdm stop or sudo lightdm stop` or
`sudo service gdm stop`

Enter runlevel 3 by typing sudo init 3
Install your` *.run` file.

`chmod +x ./your-nvidia-file.run`

You might be required to reboot when the installation finishes. If not, run 
`sudo service lightdm start or sudo start lightdm` to start your X server again.

**************
Run Installer
****************

`sudo sh NVIDIA-Linux-x86_64-$DRIVER_VERSION.run`

*****************
Package Manager
****************

In some cases, you may need to install some additional dependencies that arerequired for installing the NVIDIA drivers.

`$ sudo dnf install -y tar bzip2 make automake gcc gcc-c++ pciutils elfutils-libelf-devel libglvnd-devel iptables firewalld vim bind-utils wget`

2.Satisfy the external dependency on EPEL for DKMS.
`$ sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm`

3.Install the CUDA repository public GPG key.
`$ distribution=rhel7`

4.Setup the CUDA network repository.

`$ ARCH=$( /bin/arch )`
`$ sudo yum-config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/$distribution/${ARCH}/cuda-$distribution.repo`

 5.The NVIDIA driver requires that the kernel headers and development packages


For CentOS 7, ensure that the system has the correct Linux kernel sources from theCentOS repositories:
`$ sudo yum install -y kernel-devel-$(uname -r) kernel-headers-$(uname -r)`


 6.Update the repository cache and install the driver using the nvidia-driver-latest-dkms meta-package.
`$ sudo yum clean expire-cache`
`$ sudo yum install -y nvidia-driver-latest-dkms`


7.Post Cuda:
Environment SetUp:
To add this path to the PATH variable:

`$ export PATH=/usr/local/cuda-11.3/bin${PATH:+:${PATH}}`

In addition, when using the runfile installation method, the LD_LIBRARY_PATH variable needs to contain /usr/local/cuda-11.3/lib64 on a 64-bit system, or /usr/local/cuda-11.3/lib on a 32-bit system

 To change the environment variables for 64-bit operating systems:

`$ export LD_LIBRARY_PATH=/usr/local/cuda-11.3/lib64\
                             ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}`


***********************************************
Docker Way, if connected to stable network
***********************************************

*********************************************
cuda on centos7
********************************************
1]Download sources/rpms/run- https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=CentOS&target_version=7&target_type=rpm_network

2]Process:
`sudo sh cuda_11.3.1_465.19.01_linux.run`                        
===========
= Summary =
===========

Driver:   Installed
Toolkit:  Installed in /usr/local/cuda-11.3/
Samples:  Installed in /home/mshukla/

Please make sure that
 -   PATH includes /usr/local/cuda-11.3/bin
 -   LD_LIBRARY_PATH includes /usr/local/cuda-11.3/lib64, or, add /usr/local/cuda-11.3/lib64 to /etc/ld.so.conf and run ldconfig as root

To uninstall the CUDA Toolkit, run cuda-uninstaller in /usr/local/cuda-11.3/bin
To uninstall the NVIDIA Driver, run nvidia-uninstall
Logfile is /var/log/cuda-installer.log

3] Add to path
`export PATH=/usr/local/cuda-11.3/bin${PATH:+:${PATH}}`

`export LD_LIBRARY_PATH=/usr/local/cuda-11.3/lib64 ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}`

4] Run devicequery from samples
Install samples using `cuda-install-samples-11.3.sh <dir>`

Go to `1_utitlities`

`cd deviceQuery`

`make`

`./deviceQuery`

You will all info about device

Driver path - 
filename:       `/lib/modules/3.10.0-1160.25.1.el7.x86_64/kernel/drivers/video/nvidia-drm.ko`




********************************************
CuSignal Installation and use
*****************************************


1. Clone the repository

# Set the location to cuSignal in an environment variable CUSIGNAL_HOME
export CUSIGNAL_HOME=$(pwd)/cusignal

# Download the cuSignal repo
`git clone https://github.com/rapidsai/cusignal.git $CUSIGNAL_HOME`

Download and install Anaconda or Miniconda then create the cuSignal conda environment:

Base environment (core dependencies for cuSignal)

`cd $CUSIGNAL_HOME`
`conda env create -f conda/environments/cusignal_base.yml`

Full environment (including RAPIDS's cuDF, cuML, cuGraph, and PyTorch)

cd $CUSIGNAL_HOME
`conda env create -f conda/environments/cusignal_full.yml`

Activate conda environment

`conda activate cusignal-dev`

Install cuSignal module

cd $CUSIGNAL_HOME
./build.sh  # install cuSignal to $PREFIX if set, otherwise $CONDA_PREFIX
            # run ./build.sh -h to print the supported command line options.


Once installed, periodically update environment

cd $CUSIGNAL_HOME
conda env update -f conda/environments/cusignal_base.yml
        


Enable conda environment with Jupyter
+++++++++++++++++++++++++++++++++++++++++++


ssh tunneling from client:
`ssh -L <port number>:localhost:<port number> <server>

run cusignal env

`conda activate cusignal-dev`

Run jupyter notebook from inside env

`jupyter-notebook --no-browser --port <port number>` 

If Jupyter notebook doesn't work, check if jupyter in conda env is imstalled 

 `conda install ipython`

 `conda install jupyter` 

check by
  `import cusignal`  
 






