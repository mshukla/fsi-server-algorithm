Requirements and Libraries
===============================
1. PyCuda=2021.2
2. CuPy
3. CuSignal
4. lmfit
5. matplotlib
6. scipy
7. numpy

Conda env for development with Jupyter Notebook

Notebooks
============

Currently the code is developed in environment Jupyter Notebooks
running on Server and accessed by SSH Tunneling and on specific ports

1. Linear-Peaks-Distance.ipynb  - Algorithm running on CPU with access to offline data
2. Linear-Peaks-Distance-GPU.ipynb -Algorithm running on GPU and CPU both 
3. GPU-Linearization-Interpolation.ipynb  
   - Filter , Linearize Reference Signal and Gas Cell signal & Measured signal interpolated with   Linearized Refernce signal  
   - Execution Time comparison and Signal plots
4. FSI-GPU.ipynb
   - Most of the Algorithm running on GPU and results obtained and analysis of execution time
5. GPU-Butter-Worth-Filter-comparison.ipynb
   - Implementation of Butter Worth and Linear Filter on GPU for Reference Signal
   - Execution Time comparison and Signal plots
6. GPU-Peak-Detection - Gas Cell Peak Detection on GPU
 
Filters and Signal Processing Algorithms implemented for GPU
=============================================================
1. ButterWorth-HighPass-Filter.ipynb
2. Hilbert-Transform-GPU.ipynb
3. Spline-Interpolation-GPU.ipynb
4. Savgol-Filter-GPU.ipynb

Corresponding Python code is also available for each notebooks

Pyfsi Library
================

Library developed by GM section

